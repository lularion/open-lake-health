# Generated by Django 3.2.7 on 2021-10-03 16:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('openlake_core', '0003_auto_20211001_1711'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60)),
                ('firstname', models.CharField(max_length=60)),
                ('email', models.EmailField(max_length=100)),
                ('city', models.CharField(max_length=60)),
                ('phone', models.CharField(max_length=60)),
            ],
        ),
        migrations.RenameModel(
            old_name='Donnee',
            new_name='Data',
        ),
        migrations.RenameModel(
            old_name='Lac',
            new_name='Lake',
        ),
        migrations.DeleteModel(
            name='Utilisateur',
        ),
        migrations.RenameField(
            model_name='analyse',
            old_name='lac',
            new_name='Lake',
        ),
        migrations.RenameField(
            model_name='contributeur',
            old_name='nom',
            new_name='city',
        ),
        migrations.RenameField(
            model_name='contributeur',
            old_name='prenom',
            new_name='firstname',
        ),
        migrations.RenameField(
            model_name='contributeur',
            old_name='telephone',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='contributeur',
            old_name='ville',
            new_name='phone',
        ),
        migrations.RenameField(
            model_name='drone',
            old_name='nomdrone',
            new_name='namedrone',
        ),
        migrations.RenameField(
            model_name='lake',
            old_name='adresselac',
            new_name='addressLake',
        ),
        migrations.RenameField(
            model_name='lake',
            old_name='nomlac',
            new_name='nameLake',
        ),
        migrations.RenameField(
            model_name='pilote',
            old_name='nom',
            new_name='firstname',
        ),
        migrations.RenameField(
            model_name='pilote',
            old_name='prenom',
            new_name='name',
        ),
    ]

# Generated by Django 3.2.7 on 2021-10-08 13:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('openlake_core', '0012_auto_20211008_1257'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Analyse',
            new_name='Analysis',
        ),
        migrations.RenameField(
            model_name='data',
            old_name='analyse',
            new_name='analysis',
        ),
        migrations.RenameField(
            model_name='data',
            old_name='dataconductivité',
            new_name='conductivity',
        ),
        migrations.RenameField(
            model_name='data',
            old_name='dataph',
            new_name='ph',
        ),
        migrations.RenameField(
            model_name='data',
            old_name='datatemperature',
            new_name='temperature',
        ),
    ]

# Generated by Django 3.2.7 on 2021-10-06 19:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('openlake_core', '0008_auto_20211006_1910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='data',
            name='freqbat',
            field=models.DecimalField(decimal_places=2, max_digits=8),
        ),
    ]
